#-------------------------------------------------
#
# Project created by QtCreator 2019-07-09T13:31:13
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CameraClient
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    frmmain.cpp \
    storgewid.cpp \
    topbar.cpp \
    bottomwid.cpp \
    tabwid.cpp \
    dbutil.cpp \
    deviceinfo.cpp \
    userinfo.cpp

HEADERS += \
        mainwindow.h \
    frmmain.h \
    storgewid.h \
    topbar.h \
    bottomwid.h \
    tabwid.h \
    dbutil.h \
    deviceinfo.h \
    userinfo.h

FORMS += \
        mainwindow.ui \
    frmmain.ui \
    storgewid.ui \
    topbar.ui \
    bottomwid.ui \
    tabwid.ui

INCLUDEPATH += D:\hk\include
INCLUDEPATH += $$PWD/sdk/include

LIBS += -LD:\hk\lib\ -lGdiPlus -lHCCore -lHCNetSDK -lPlayCtrl
LIBS += -LD:\hk\lib\HCNetSDKCom -lHCGeneralCfgMgr -lHCPreview


CONFIG(release, debug|release){
LIBS        += -L$$PWD/sdk/ -lquc
}

RESOURCES += \
    res.qrc \
    icon.qrc

unix|win32: LIBS += -LD:/opencvV14/opencv/build/x64/vc14/lib/ -lopencv_world345

INCLUDEPATH += D:/opencvV14/opencv/build/include
DEPENDPATH += D:/opencvV14/opencv/build/include

unix|win32: LIBS += -LD:/ArcSoft_ArcFace_Windows_x64_V2.2/lib/X64/ -llibarcsoft_face_engine

INCLUDEPATH += D:/ArcSoft_ArcFace_Windows_x64_V2.2/inc
DEPENDPATH += D:/ArcSoft_ArcFace_Windows_x64_V2.2/inc
