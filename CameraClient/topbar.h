﻿#ifndef TOPBAR_H
#define TOPBAR_H

#include <QWidget>

namespace Ui {
class TopBar;
}

class TopBar : public QWidget
{
    Q_OBJECT

public:
    explicit TopBar(QWidget *parent = nullptr);
    ~TopBar();
    /*
     * 设置图标路径
     */
    void setIcon(const QString& iconPath);
    void setIcon(const QPixmap& icon);

    /*
     * 设置标题名字
     */
    void setName(const QString& name);

    /*
     */

    void setBtnIconSize(QSize s);
    void setWindIconSize(QSize s);

signals:

    void exit();
    void maximize();
    void minimize();
    void lockCliked();
    void arrowClicked();
    void userClicked();
    void screenCliked();
    void listCliked();

private slots:

    void on_btnExit_clicked();

    void on_btnMax_clicked();

    void on_btnMin_clicked();

    void on_btnLock_clicked();

    void on_btnArrow_clicked();

    void on_btnUser_clicked();

    void on_btnScreen_clicked();

    void on_btnList_clicked();

private:
    Ui::TopBar *ui;
};

#endif // TOPBAR_H
