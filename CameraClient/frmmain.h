﻿#ifndef FRMMAIN_H
#define FRMMAIN_H

#include <QtGui>
#if (QT_VERSION > QT_VERSION_CHECK(5,0,0))
#include <QtWidgets>
#include "HCNetSDK.h"
#include "plaympeg4.h"
//#include "DecodeCardSdk.h"
#include "plaympeg4.h"
#include <QSqlDatabase>
#include "dbutil.h"
#include <QLabel>




#include <stdio.h>
#include <stdlib.h>
#include "arcsoft_face_sdk.h"
#include "amcomdef.h"
#include "asvloffscreen.h"
#include "merror.h"
#include <direct.h>
#include <iostream>
#include <stdarg.h>
#include <string>
#include <opencv2\opencv.hpp>

#include<opencv\highgui.h>
#define APPID "4XUD9uTwCLqE98s1yzMPLi2qYp5SBA5CeiAER68TaYMQ"
#define SDKKey "Hytvmc8gboGiyak9gzSTx5AoTJmw52yWWTTgYghvjUVU"


#define SafeFree(p) { if ((p)) free(p); (p) = NULL; }
#define SafeArrayDelete(p) { if ((p)) delete [] (p); (p) = NULL; }
#define SafeDelete(p) { if ((p)) delete (p); (p) = NULL; }



#endif

namespace Ui
{
class frmMain;
}

class frmMain : public QWidget
{
    Q_OBJECT

public:
    explicit frmMain(QWidget *parent = 0);
    ~frmMain();
    QList<QLabel *> getScreen();//返回分屏
    void setInfomation(QString objName,int flag);//设置提示图片 flag=0不设置 1无连接 2错误
    void initEv();

    QSqlDatabase db;
    DBUtil dbUtil;
    QList<DeviceInfo*>deviceInfoList;
    QList<QLabel*>screenList;
    long play(long userID, HWND hWnd, NET_DVR_PREVIEWINFO struPlayInfo);
    void SB(QString path);
    MHandle handle;
    QList<long>deviceHandle;
    QMap<QLabel*,long>deviceMap;//维护一个map 记录label和当前摄像头句柄
    QLabel *curScreen=NULL;//记录当前全屏的label
protected:
    bool eventFilter(QObject *watched, QEvent *event);

private:
    Ui::frmMain *ui;

    bool videoMax;
    int videoCount;
    QString videoType;
    QMenu *videoMenu;
    QList<QLabel *> widgets;



    void CutIplImage(IplImage* src, IplImage* dst, int x, int y);
    void  initEngine();


private slots:
    void initForm();
    void initMenu();
    void initFVideo(int ch=4);

private slots:
    void play_video_all();
    void snapshot_video_one();
    void snapshot_video_all();

    void show_video_all();
    void show_video_4();
    void show_video_6();
    void show_video_8();
    void show_video_9();
    void show_video_16();

    void hide_video_all();
    void change_video(int index, int flag);
    void change_video_4(int index);
    void change_video_6(int index);
    void change_video_8(int index);
    void change_video_9(int index);
    void change_video_16(int index);
};

#endif // FRMMAIN_H
