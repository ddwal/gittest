﻿#ifndef BOTTOMWID_H
#define BOTTOMWID_H

#include <QWidget>

namespace Ui {
class BottomWid;
}

class BottomWid : public QWidget
{
    Q_OBJECT

public:
    explicit BottomWid(QWidget *parent = nullptr);
    ~BottomWid();
    void setBtnIconSize(QSize s);
signals:
    void muteClicked();
    void saveClicked();
    void closeClicked();
    void flowClicked();
    void fullClickd();
    void settingCLicked();


private slots:

    void on_btnSetting_clicked();

    void on_btnFull_clicked();

    void on_btnFlow_clicked();

    void on_btnClose_clicked();

    void on_btnSave_clicked();

    void on_btnMute_clicked();

private:
    Ui::BottomWid *ui;
};

#endif // BOTTOMWID_H
