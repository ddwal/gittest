﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPalette>
#include <QTreeWidget>

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //绑定信号槽
    connect(ui->storgeWid,SIGNAL(storge()),this,SLOT(storgeSlot()));
    connect(ui->topBar,SIGNAL(maximize()),this,SLOT(showMaximizedSlot()));
    connect(ui->topBar,SIGNAL(minimize()),this,SLOT(showMinimized()));
    connect(ui->topBar,SIGNAL(exit()),this,SLOT(close()));
    //窗口无边框
    this->setWindowFlags(Qt::FramelessWindowHint | windowFlags());

    initForm();
    initBgColor();
    initCamera();




}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mouseMoveEvent(QMouseEvent *ev)//鼠标移动事件
{
    if(ev->pos().x()<=ui->topBar->x()+ui->topBar->width()
            && ev->pos().y()<=ui->topBar->y()+ui->topBar->height())
    {
        if(ev->buttons() == Qt::LeftButton)
        {
            move(ev->globalPos()-disPos);
            ev->accept();
        }
    }


}

void MainWindow::mousePressEvent(QMouseEvent *ev)//鼠标点击事件
{
    if(ev->button()==Qt::LeftButton)
    {
        curPos = this->pos();
        winPos = ev->globalPos();
        disPos = winPos-curPos;
        ev->accept();
    }
}

void MainWindow::resizeEvent(QResizeEvent *event)
{

    if(this->isMaximized())
    {
        isMax = true;

    }else
    {
        isMax = false;
    }
    normalRect =  this->rect();
//    qDebug()<<isMax;

}

void MainWindow::initForm()
{
    //设置标题栏图标
    ui->topBar->setIcon(QPixmap(":/new/icon/icon/icon/windowIcon.png"));
    ui->topBar->setWindIconSize(QSize(24,24));
    connect(ui->tabBar,SIGNAL(btnCLick(QString)),this,SLOT(navClickSlot(QString)));

}

void MainWindow::initBgColor()
{
    //设置顶部栏背景
    ui->topBar->setAutoFillBackground(true);
    QPalette p = ui->topBar->palette();
    QBrush bgColor(QColor(19,21,23));
    p.setBrush(QPalette::Background,bgColor);
    ui->topBar->setPalette(p);

    //设置功能栏背景
    ui->tabBar->setAutoFillBackground(true);
    p = ui->tabBar->palette();
    bgColor.setColor(QColor(22,23,26));
    p.setBrush(QPalette::Background,bgColor);
    ui->tabBar->setPalette(p);

    //设置底部栏背景
    ui->bottomWid->setAutoFillBackground(true);
    p = ui->bottomWid->palette();
    bgColor.setColor(QColor(46,49,54));
    p.setBrush(QPalette::Background,bgColor);
    ui->bottomWid->setPalette(p);

    ui->storgeWid->setAutoFillBackground(true);
    p = ui->storgeWid->palette();
    bgColor.setColor(QColor(54,57,62));
    p.setBrush(QPalette::Background,bgColor);
    ui->storgeWid->setPalette(p);


    ui->topBar->setBtnIconSize(QSize(16,16));
    ui->bottomWid->setBtnIconSize(QSize(20,20));

}

void MainWindow::move(int x, int y)
{
    if(this->isMaximized())
    {
        if(y>1)
        {
            this->showNormal();
        }
    }
}

void MainWindow::move(QPoint p)
{

//    qDebug()<<p;
    this->setGeometry(p.x(),p.y(),this->width(),this->height());
    if(isMax)
    {
        this->setGeometry(normalRect);
        isMax = false;
    }

}


void MainWindow::initCamera()
{
    ui->treeWidget->clear();
    QTreeWidgetItem *cGroup1 = new QTreeWidgetItem(ui->treeWidget,QStringList()<<QString::fromLocal8Bit("分组1"));

    QTreeWidgetItem *cItem1_1 = new QTreeWidgetItem(cGroup1,QStringList()<<QString::fromLocal8Bit("摄像头1"));
    QTreeWidgetItem *cItem1_2 = new QTreeWidgetItem(cGroup1,QStringList()<<QString::fromLocal8Bit("摄像头2"));
    QTreeWidgetItem *cItem1_3 = new QTreeWidgetItem(cGroup1,QStringList()<<QString::fromLocal8Bit("摄像头3"));


    QTreeWidgetItem *cGroup2 = new QTreeWidgetItem(ui->treeWidget,QStringList()<<QString::fromLocal8Bit("分组2"));
    QTreeWidgetItem *cItem2_1 = new QTreeWidgetItem(cGroup2,QStringList()<<QString::fromLocal8Bit("摄像头4"));
    QTreeWidgetItem *cItem2_2 = new QTreeWidgetItem(cGroup2,QStringList()<<QString::fromLocal8Bit("摄像头5"));
    QTreeWidgetItem *cItem2_3 = new QTreeWidgetItem(cGroup2,QStringList()<<QString::fromLocal8Bit("摄像头6"));

    QTreeWidgetItem *cGroup3 = new QTreeWidgetItem(ui->treeWidget,QStringList()<<QString::fromLocal8Bit("分组3"));
    QTreeWidgetItem *cItem3_1 = new QTreeWidgetItem(cGroup3,QStringList()<<QString::fromLocal8Bit("摄像头7"));
    QTreeWidgetItem *cItem3_2 = new QTreeWidgetItem(cGroup3,QStringList()<<QString::fromLocal8Bit("摄像头8"));
    QTreeWidgetItem *cItem3_3 = new QTreeWidgetItem(cGroup3,QStringList()<<QString::fromLocal8Bit("摄像头9"));

    cGroup1->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/group.png"));
    cGroup2->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/group.png"));
    cGroup3->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/group.png"));

    cItem1_1->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/camera.png"));
    cItem1_2->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/camera.png"));
    cItem1_3->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/camera.png"));

    cItem2_1->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/camera.png"));
    cItem2_2->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/camera.png"));
    cItem2_3->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/camera.png"));

    cItem3_1->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/camera.png"));
    cItem3_2->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/camera.png"));
    cItem3_3->setIcon(0,QIcon(":/new/navIcon/icon/navIcon/camera.png"));

    ui->treeWidget->expandAll();
}

void MainWindow::storgeSlot()
{
    if(ui->navWid->isHidden())
    {
        ui->navWid->show();
    }else
    {
        ui->navWid->hide();
    }
}

void MainWindow::showMaximizedSlot()
{
    if(this->isMaximized())
    {
        this->showNormal();

    }else
    {
        this->showMaximized();
    }
}

void MainWindow::navClickSlot(QString name)
{

    qDebug()<<name;

    if(name==QString::fromLocal8Bit("主预览"))
    {
        qDebug()<<1;
        ui->stackedWidget->setCurrentIndex(0);

    }else if(name==QString::fromLocal8Bit("功能1"))
    {
        qDebug()<<2;
        ui->stackedWidget->setCurrentIndex(1);
    }else if(name==QString::fromLocal8Bit("功能2"))
    {
        qDebug()<<3;
        ui->stackedWidget->setCurrentIndex(2);

    }else
    {
        qDebug()<<5;
    }

}



void MainWindow::on_pushButton_clicked()
{
    ui->widgetCamera->SB("aaa");
}
