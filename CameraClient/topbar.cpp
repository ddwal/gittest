﻿#include "topbar.h"
#include "ui_topbar.h"

TopBar::TopBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TopBar)
{
    ui->setupUi(this);
    // 隐藏标题栏
    this->setWindowFlags(Qt::FramelessWindowHint);


}

TopBar::~TopBar()
{
    delete ui;
}

void TopBar::setIcon(const QString& iconPath)
{
    setIcon(QPixmap(iconPath));
}

void TopBar::setIcon(const QPixmap& icon)
{
    ui->labelIcon->setPixmap(icon);
    ui->labelIcon->setScaledContents(true);
    ui->labelIcon->show();
}

void TopBar::setName(const QString& name)
{
    ui->labelName->setText(name);
}

void TopBar::setBtnIconSize(QSize s)
{
    QList<QToolButton*>btnList = this->findChildren<QToolButton*>();
    for(QToolButton *tBtn:btnList)
    {
        tBtn->setIconSize(s);
        tBtn->setMinimumSize(s);
        tBtn->setMaximumSize(s);
    }
}

void TopBar::setWindIconSize(QSize s)
{
    ui->labelIcon->setMinimumSize(s);
    ui->labelIcon->setMaximumSize(s);
}

void TopBar::on_btnExit_clicked()
{
    emit exit();
}

void TopBar::on_btnMax_clicked()
{
    emit maximize();
}

void TopBar::on_btnMin_clicked()
{
    emit minimize();

}

void TopBar::on_btnLock_clicked()
{
    emit lockCliked();
}

void TopBar::on_btnArrow_clicked()
{
    emit arrowClicked();
}

void TopBar::on_btnUser_clicked()
{
    emit userClicked();
}

void TopBar::on_btnScreen_clicked()
{
    emit screenCliked();
}


void TopBar::on_btnList_clicked()
{
    emit listCliked();
}
