﻿#include "bottomwid.h"
#include "ui_bottomwid.h"

BottomWid::BottomWid(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BottomWid)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

BottomWid::~BottomWid()
{
    delete ui;
}

void BottomWid::setBtnIconSize(QSize s)
{
    QList<QToolButton*>btnList = this->findChildren<QToolButton*>();
    for(QToolButton * tBtn:btnList)
    {
        tBtn->setIconSize(s);
        tBtn->setMinimumSize(s);
        tBtn->setMaximumSize(s);
    }
}

void BottomWid::on_btnSetting_clicked()
{
    emit settingCLicked();
}

void BottomWid::on_btnFull_clicked()
{
    emit fullClickd();
}

void BottomWid::on_btnFlow_clicked()
{
    emit flowClicked();
}

void BottomWid::on_btnClose_clicked()
{
    emit closeClicked();
}

void BottomWid::on_btnSave_clicked()
{
    emit saveClicked();
}

void BottomWid::on_btnMute_clicked()
{
    emit muteClicked();
}
