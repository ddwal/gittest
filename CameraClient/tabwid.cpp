﻿#include "tabwid.h"
#include "ui_tabwid.h"
#include "navbutton.h"

TabWid::TabWid(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TabWid)
{
    ui->setupUi(this);
    QList<QPushButton*>navGroup = this->findChildren<QPushButton*>();
    if(!navGroup.empty())//设置第一个按钮的状态
    {
        navGroup.at(0)->setChecked(true);
    }
    for(QPushButton* tNav:navGroup)//修改按钮字体的颜色
    {
        NavButton* tmpNav = (NavButton*)tNav;
        connect(tmpNav,SIGNAL(clicked(bool)),this,SLOT(click()));
        //显示三角
        tmpNav->setShowTriangle(true);
        tmpNav->setAutoExclusive(true);
        tmpNav->setNormalBgColor(QColor(59,60,63));
        tmpNav->setLinePosition(NavButton::LinePosition_Bottom);
        tmpNav->setLineColor(QColor(192,15,21));
    }


}

TabWid::~TabWid()
{
    delete ui;
}



void TabWid::click()
{
    emit btnCLick(((QPushButton*)sender())->text());//点击后 发出带有按钮标题的信号
}
