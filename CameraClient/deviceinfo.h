﻿#ifndef DEVICEINFO_H
#define DEVICEINFO_H
#include <QString>

class DeviceInfo
{
public:
    DeviceInfo();
    int     getId();
    QString getDeviceAddress();
    int     getDevicePort();
    QString getDeviceUsername();
    QString getDevicePassword();
    QString getStreamAddress();

    void    setId(int id);
    void    setDeviceAddress(QString deviceAddress);
    void    setDevicePort(int port);
    void    setDeviceUsername(QString deviceUsername);
    void    setDevicePassword(QString devicePassword);
    void    setStreamAddress(QString streamAddress);
    QString toString();

private:
    int id;
    QString deviceAddress;
    int devicePort;
    QString deviceUsername;
    QString devicePassword;
    QString streamAddress;


};

#endif // DEVICEINFO_H
