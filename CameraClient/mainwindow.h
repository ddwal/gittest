﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QMoveEvent>
#include <QMouseEvent>
#include <QResizeEvent>
#include "navbutton.h"
#include <QSqlDatabase>
#include "userinfo.h"
#include "deviceinfo.h"
#include "dbutil.h"
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    //用来实现窗口移动的


    void initForm();//初始化控件
    void initBgColor();
    void move(int x, int y);
    void move(QPoint p);
private:

    QList<QLabel*>screenList;

    QPoint winPos;
    QPoint curPos;
    QPoint disPos;
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void resizeEvent(QResizeEvent *event);
    bool isMax = false;
//    void initCamera();
    QRect normalRect;

    void initCamera();





public slots:
    void storgeSlot();//缩放左边栏
    void showMaximizedSlot();//响应最大化和回去
    void navClickSlot(QString name);//用于根据tabWid返回的按钮名 判定是哪个按钮点击了





private slots:


    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
