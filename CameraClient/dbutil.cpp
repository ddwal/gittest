﻿#include "dbutil.h"
#include <QDebug>
//QSqlDatabase DBUtil::db;
//QSqlQuery DBUtil::query;
//bool DBUtil::isCon = false;
//QList<DeviceInfo*> DBUtil::DeviceInfoes;
DBUtil::DBUtil()
{

}

void DBUtil::initDb(const QString path, const QString type)
{

}

bool DBUtil::initDb(const QString hostname, const QString username, const QString password, const int port, const QString dbName, const QString type)
{
    db = QSqlDatabase::addDatabase(type);
    db.setHostName(hostname);
    db.setUserName(username);
    db.setPassword(password);
    db.setPort(port);
    db.setDatabaseName(dbName);
    if(db.open())
    {
        isCon = true;
        return true;
    }else
    {
        isCon = false;
        return false;
    }

}

QSqlDatabase DBUtil::getDb()
{
    return db;
}

QString DBUtil::getLastError()
{
    QSqlError lastErr = db.lastError();
    return lastErr.databaseText();
}

QSqlQuery DBUtil::getQuery()
{
    QSqlDatabase::database();
    return query;
}

void DBUtil::closeDb()
{
    db.close();
}



QList<DeviceInfo *> DBUtil::getDeviceInfo()
{
    DeviceInfoes.clear();
    QSqlDatabase::database();
    QSqlQuery que;
    que.exec("select * from deviceInfo");


    while (que.next()) {
        DeviceInfo *tmp = new DeviceInfo;
        tmp->setId( que.value("id").toInt());
        tmp->setDeviceAddress(que.value("deviceAddress").toString());
        tmp->setDevicePassword(que.value("devicePassword").toString());
        tmp->setDevicePort(que.value("devicePort").toInt());
        tmp->setDeviceUsername(que.value("deviceUsername").toString());
        DeviceInfoes.append(tmp);
    }

    return DeviceInfoes;
}

UserInfo *DBUtil::findUser(UserInfo *info)
{
    QSqlDatabase::database();
    QSqlQuery que;
    QString sql = QString("select * from userinfo where susername = '%1' and spassword = '%2'")
            .arg(info->getSUsername()).arg(info->getSPassword());
    qDebug()<<sql;
    UserInfo *tUserInfo = new UserInfo;
    bool ok = que.exec(sql);
    if(ok)
    {
       if(que.size()<=0)
       {
            return NULL;
       }

        while (que.next()) {

            tUserInfo->setId(que.value("id").toInt());
            tUserInfo->setSUsername(que.value("susername").toString());
            tUserInfo->setSPassword(que.value("spassword").toString());
            tUserInfo->setSLastSignTime(que.value("slastsigntime").toString());
        }
        return tUserInfo;

    }else
    {
        return NULL;
    }


}



