﻿#ifndef STORGEWID_H
#define STORGEWID_H

#include <QWidget>

namespace Ui {
class StorgeWid;
}

class StorgeWid : public QWidget
{
    Q_OBJECT

public:
    explicit StorgeWid(QWidget *parent = 0);
    ~StorgeWid();
    int i =0;

private slots:
    void on_toolButtonStorge_clicked();
signals:
    void storge();

private:
    Ui::StorgeWid *ui;
};

#endif // STORGEWID_H
