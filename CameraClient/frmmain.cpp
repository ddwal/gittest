﻿#include "frmmain.h"
#include "ui_frmmain.h"
#include <Windows.h>
#include <QDebug>

#pragma execution_character_set("utf-8")

frmMain::frmMain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::frmMain)
{
    ui->setupUi(this);
    this->initForm();
    this->initMenu();

    initFVideo(4);
    initEv();
    initEngine();

    QTimer::singleShot(1000, this, SLOT(play_video_all()));
}

frmMain::~frmMain()
{
    delete ui;
}

QList<QLabel *> frmMain::getScreen()
{
    return widgets;
}

void frmMain::setInfomation(QString objName, int flag)
{
    for(QLabel *tLab:widgets)
    {
        if(tLab->objectName()==objName)
        {
            if(flag==0)
            {
                tLab->setPixmap(QPixmap(""));

            }else if(flag==1)
            {
                tLab->setPixmap(QPixmap(":/new/prefix1/res/noSignal.png"));
            }else if(flag==2)
            {
                tLab->setPixmap(QPixmap(":/new/prefix1/res/error.png"));
            }
        }
    }
}

void frmMain::initEv()
{
    NET_DVR_Init();
    NET_DVR_SetConnectTime(2000, 1);
    NET_DVR_SetReconnect(10000, true);
    bool con = dbUtil.initDb("127.0.0.1","cameradb","s123456",3306,"cameradb","QMYSQL");
    qDebug()<<"数据库连接："<<con;
}

long frmMain::play(long userID, HWND hWnd, NET_DVR_PREVIEWINFO struPlayInfo)
{
    struPlayInfo={0};
    struPlayInfo.hPlayWnd = hWnd;//需要 SDK 解码时句柄设为有效值，仅取流不解码时可设为空
    struPlayInfo.lChannel = 1;//预览通道号
    struPlayInfo.dwStreamType = 0;//码流类型：0-主码流，1-子码流，2-三码流，3-虚拟码流，以此类推
    struPlayInfo.dwLinkMode = 0;//0- TCP 方式，1- UDP 方式，2- 多播方式，3- RTP 方式，4-RTP/RTSP，5-RSTP/HTTP
    struPlayInfo.bBlocked = 1;//0- 非阻塞取流，1- 阻塞取流


    long IRealPlayHandle = NET_DVR_RealPlay_V40(userID,&struPlayInfo,NULL,NULL);



    if(IRealPlayHandle <0)
    {
        qDebug() << "NET_DVR_RealPlay_V40 error;error number " <<NET_DVR_GetLastError();
        NET_DVR_Logout(userID);
        return 0;
    }
    else
        return IRealPlayHandle;
}

bool frmMain::eventFilter(QObject *watched, QEvent *event)
{
    QLabel *widget = (QLabel *) watched;
    if (event->type() == QEvent::MouseButtonDblClick) {

        if (!videoMax) {
            videoMax = true;
            hide_video_all();
            ui->gridLayout->addWidget(widget, 0, 0);
            widget->setVisible(true);
            curScreen = widget;
        } else {
            videoMax = false;
            show_video_all();
            curScreen = NULL;
        }
    } else if (event->type() == QEvent::MouseButtonPress) {
        if (qApp->mouseButtons() == Qt::RightButton) {
            curScreen = widget;
            videoMenu->exec(QCursor::pos());

        }
    }

    return QWidget::eventFilter(watched, event);
}

void frmMain::initForm()
{
    //设置样式表
    QStringList qss;
    qss.append("QFrame{border:2px solid #000000;}");
    qss.append("QLabel{font:75 25px;color:#F0F0F0;border:1px solid #AAAAAA;background:#000000;}");
    qss.append("QLabel:focus{border:1px solid #00BB9E;background:#555555;}");
    ui->frame->setStyleSheet(qss.join(""));

    videoMax = false;
    videoCount = 16;
    videoType = "1_16";


    for (int i = 0; i < videoCount; i++) {
        QLabel *widget = new QLabel;
        widget->setObjectName(QString("video%1").arg(i + 1));
        widget->installEventFilter(this);
        widget->setFocusPolicy(Qt::StrongFocus);
        widget->setAlignment(Qt::AlignCenter);

        //二选一可以选择显示文字,也可以选择显示背景图片
        //widget->setText(QString("通道 %1").arg(i + 1));
        widget->setPixmap(QPixmap(":/new/prefix1/res/noSignal.png"));
        widgets.append(widget);
    }


}

void frmMain::initMenu()
{
    videoMenu = new QMenu(this);
    videoMenu->addAction("截图当前视频", this, SLOT(snapshot_video_one()));
    videoMenu->addAction("截图所有视频", this, SLOT(snapshot_video_all()));
    videoMenu->addSeparator();

    QMenu *menu4 = videoMenu->addMenu("切换到4画面");
    menu4->addAction("通道1-通道4", this, SLOT(show_video_4()));
    menu4->addAction("通道5-通道8", this, SLOT(show_video_4()));
    menu4->addAction("通道9-通道12", this, SLOT(show_video_4()));
    menu4->addAction("通道13-通道16", this, SLOT(show_video_4()));

    QMenu *menu6 = videoMenu->addMenu("切换到6画面");
    menu6->addAction("通道1-通道6", this, SLOT(show_video_6()));
    menu6->addAction("通道6-通道11", this, SLOT(show_video_6()));
    menu6->addAction("通道11-通道16", this, SLOT(show_video_6()));

    QMenu *menu8 = videoMenu->addMenu("切换到8画面");
    menu8->addAction("通道1-通道8", this, SLOT(show_video_8()));
    menu8->addAction("通道9-通道16", this, SLOT(show_video_8()));

    QMenu *menu9 = videoMenu->addMenu("切换到9画面");
    menu9->addAction("通道1-通道9", this, SLOT(show_video_9()));
    menu9->addAction("通道8-通道16", this, SLOT(show_video_9()));

    videoMenu->addAction("切换到16画面", this, SLOT(show_video_16()));
}

void frmMain::initFVideo(int ch)
{


    videoMax = false;
    QString videoType;
    int index = 0;
    index = 0;


    videoType = QString("1_%1").arg(ch);


    if (this->videoType != videoType) {
        this->videoType = videoType;
        switch (ch) {
        case 4:
            change_video_4(index);
            break;
        case 9:
            change_video_9(index);
            break;
        default:
            change_video_4(index);
            break;
        }

    }
}

void frmMain::play_video_all()
{
    deviceHandle.clear();
    deviceMap.clear();
    deviceInfoList = dbUtil.getDeviceInfo();
    screenList =  this->getScreen();
    qDebug()<<screenList.size();
    for(int i=0;i<deviceInfoList.size();i++)
    {
        qDebug()<<"连接第"<<i<<"个摄像头";

        DeviceInfo *tDevice = deviceInfoList.at(i);
        LONG lUserID;

        //登录参数，包括设备地址、登录用户、密码等
        NET_DVR_USER_LOGIN_INFO struLoginInfo = {0};
        struLoginInfo.bUseAsynLogin = 0; //同步登录方式
        strcpy(struLoginInfo.sDeviceAddress, tDevice->getDeviceAddress().toUtf8()); //设备IP地址
        struLoginInfo.wPort = tDevice->getDevicePort(); //设备服务端口
        strcpy(struLoginInfo.sUserName, tDevice->getDeviceUsername().toUtf8()); //设备登录用户名
        strcpy(struLoginInfo.sPassword, tDevice->getDevicePassword().toUtf8()); //设备登录密码
        qDebug()<<tDevice->getDeviceAddress().toUtf8();
        qDebug()<<tDevice->getDevicePort();
        qDebug()<<tDevice->getDeviceUsername().toUtf8();
        qDebug()<<tDevice->getDevicePassword().toUtf8();
        //设备信息, 输出参数
        NET_DVR_DEVICEINFO_V40 struDeviceInfoV40 = {0};

        lUserID = NET_DVR_Login_V40(&struLoginInfo, &struDeviceInfoV40);

       if(lUserID<0)
       {
           qDebug() << "NET_DVR_Login_V40 error;" << "error number is " << NET_DVR_GetLastError();
           return;
       }
        QLabel *tmpLabel = screenList.at(i);
       HWND hWnd = (HWND)tmpLabel->winId();//获得句柄
       tmpLabel->setPixmap(QPixmap(""));//去掉背景图
       NET_DVR_PREVIEWINFO struPlayInfo;
       long IRealPlayHandle = play(lUserID,hWnd,struPlayInfo);//获得当前播放句柄
       deviceHandle.append(IRealPlayHandle);//加入维护的map里
       NET_DVR_SetCapturePictureMode(1);
       deviceMap.insert(tmpLabel,IRealPlayHandle);


    }
}

void frmMain::snapshot_video_one()
{

}

void frmMain::snapshot_video_all()
{

}

void frmMain::show_video_all()
{
    if (videoType == "1_4") {
        change_video_4(0);
    } else if (videoType == "5_8") {
        change_video_4(4);
    } else if (videoType == "9_12") {
        change_video_4(8);
    } else if (videoType == "13_16") {
        change_video_4(12);
    } else if (videoType == "1_6") {
        change_video_6(0);
    } else if (videoType == "6_11") {
        change_video_6(5);
    } else if (videoType == "11_16") {
        change_video_6(10);
    } else if (videoType == "1_8") {
        change_video_8(0);
    } else if (videoType == "9_16") {
        change_video_8(8);
    } else if (videoType == "1_9") {
        change_video_9(0);
    } else if (videoType == "8_16") {
        change_video_9(7);
    } else if (videoType == "1_16") {
        change_video_16(0);
    }
}

void frmMain::show_video_4()
{
    videoMax = false;
    QString videoType;
    int index = 0;

    QAction *action = (QAction *)sender();
    QString name = action->text();

    if (name == "通道1-通道4") {
        index = 0;
        videoType = "1_4";
    } else if (name == "通道5-通道8") {
        index = 4;
        videoType = "5_8";
    } else if (name == "通道9-通道12") {
        index = 8;
        videoType = "9_12";
    } else if (name == "通道13-通道16") {
        index = 12;
        videoType = "13_16";
    }

    if (this->videoType != videoType) {
        this->videoType = videoType;
        change_video_4(index);
    }
}

void frmMain::show_video_6()
{
    videoMax = false;
    QString videoType;
    int index = 0;

    QAction *action = (QAction *)sender();
    QString name = action->text();

    if (name == "通道1-通道6") {
        index = 0;
        videoType = "1_6";
    } else if (name == "通道6-通道11") {
        index = 5;
        videoType = "6_11";
    } else if (name == "通道11-通道16") {
        index = 10;
        videoType = "11_16";
    }

    if (this->videoType != videoType) {
        this->videoType = videoType;
        change_video_6(index);
    }
}

void frmMain::show_video_8()
{
    videoMax = false;
    QString videoType;
    int index = 0;

    QAction *action = (QAction *)sender();
    QString name = action->text();

    if (name == "通道1-通道8") {
        index = 0;
        videoType = "1_8";
    } else if (name == "通道9-通道16") {
        index = 8;
        videoType = "9_16";
    }

    if (this->videoType != videoType) {
        this->videoType = videoType;
        change_video_8(index);
    }
}

void frmMain::show_video_9()
{
    videoMax = false;
    QString videoType;
    int index = 0;

    QAction *action = (QAction *)sender();
    QString name = action->text();

    if (name == "通道1-通道9") {
        index = 0;
        videoType = "1_9";
    } else if (name == "通道8-通道16") {
        index = 7;
        videoType = "8_16";
    }

    if (this->videoType != videoType) {
        this->videoType = videoType;
        change_video_9(index);
    }
}

void frmMain::show_video_16()
{
    videoMax = false;
    QString videoType;
    int index = 0;
    videoType = "1_16";

    if (this->videoType != videoType) {
        this->videoType = videoType;
        change_video_16(index);
    }
}

void frmMain::hide_video_all()
{
    for (int i = 0; i < videoCount; i++) {
        ui->gridLayout->removeWidget(widgets.at(i));
        widgets.at(i)->setVisible(false);
    }
}

void frmMain::change_video(int index, int flag)
{
    int count = 0;
    int row = 0;
    int column = 0;

    for (int i = 0; i < videoCount; i++) {
        if (i >= index) {
            ui->gridLayout->addWidget(widgets.at(i), row, column);
            widgets.at(i)->setVisible(true);

            count++;
            column++;
            if (column == flag) {
                row++;
                column = 0;
            }
        }

        if (count == (flag * flag)) {
            break;
        }
    }
}

void frmMain::change_video_4(int index)
{
    hide_video_all();
    change_video(index, 2);
}

void frmMain::change_video_6(int index)
{
    hide_video_all();
    if (index == 0) {
        ui->gridLayout->addWidget(widgets.at(0), 0, 0, 2, 2);
        ui->gridLayout->addWidget(widgets.at(1), 0, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(2), 1, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(3), 2, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(4), 2, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(5), 2, 0, 1, 1);

        for (int i = 0; i < 6; i++) {
            widgets.at(i)->setVisible(true);
        }
    } else if (index == 5) {
        ui->gridLayout->addWidget(widgets.at(5), 0, 0, 2, 2);
        ui->gridLayout->addWidget(widgets.at(6), 0, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(7), 1, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(8), 2, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(9), 2, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(10), 2, 0, 1, 1);

        for (int i = 5; i < 11; i++) {
            widgets.at(i)->setVisible(true);
        }
    } else if (index == 10) {
        ui->gridLayout->addWidget(widgets.at(10), 0, 0, 2, 2);
        ui->gridLayout->addWidget(widgets.at(11), 0, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(12), 1, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(13), 2, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(14), 2, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(15), 2, 0, 1, 1);

        for (int i = 10; i < 16; i++) {
            widgets.at(i)->setVisible(true);
        }
    }
}

void frmMain::change_video_8(int index)
{
    hide_video_all();
    if (index == 0) {
        ui->gridLayout->addWidget(widgets.at(0), 0, 0, 3, 3);
        ui->gridLayout->addWidget(widgets.at(1), 0, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(2), 1, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(3), 2, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(4), 3, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(5), 3, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(6), 3, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(7), 3, 0, 1, 1);

        for (int i = 0; i < 8; i++) {
            widgets.at(i)->setVisible(true);
        }
    } else if (index == 8) {
        ui->gridLayout->addWidget(widgets.at(8), 0, 0, 3, 3);
        ui->gridLayout->addWidget(widgets.at(9), 0, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(10), 1, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(11), 2, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(12), 3, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(13), 3, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(14), 3, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(15), 3, 0, 1, 1);

        for (int i = 8; i < 16; i++) {
            widgets.at(i)->setVisible(true);
        }
    }
}

void frmMain::change_video_9(int index)
{
    hide_video_all();
    change_video(index, 3);
}

void frmMain::change_video_16(int index)
{
    hide_video_all();
    change_video(index, 4);
}


//裁剪图片,做四字节对齐
void frmMain::CutIplImage(IplImage* src, IplImage* dst, int x, int y)
{
    CvSize size = cvSize(dst->width, dst->height);//区域大小
    cvSetImageROI(src, cvRect(x, y, size.width, size.height));//设置源图像ROI
    cvCopy(src, dst); //复制图像
    cvResetImageROI(src);//源图像用完后，清空ROI
}

void frmMain::initEngine()
{
    //激活接口,需联网激活
    MRESULT res = ASFOnlineActivation(const_cast<char*>(APPID), const_cast<char*>(SDKKey));
    //MRESULT res = ASFActivation(APPID, SDKKey);
    if (MOK != res && MERR_ASF_ALREADY_ACTIVATED != res)
        printf("ASFActivation fail: %d\n", res);
    else
        printf("ASFActivation sucess: %d\n", res);

    //获取激活文件信息
    ASF_ActiveFileInfo  activeFileInfo;
    res = ASFGetActiveFileInfo(&activeFileInfo);
    if (res != MOK)
        printf("ASFGetActiveFileInfo fail: %d\n", res);

    //初始化接口
    handle = NULL;
    MInt32 mask = ASF_FACE_DETECT | ASF_FACERECOGNITION | ASF_AGE | ASF_GENDER | ASF_FACE3DANGLE | ASF_LIVENESS | ASF_IR_LIVENESS;
    res = ASFInitEngine(ASF_DETECT_MODE_IMAGE, ASF_OP_0_ONLY, 30, 10, mask, &handle);
    if (res != MOK)
        printf("ASFInitEngine fail: %d\n", res);
    else
        printf("ASFInitEngine sucess: %d\n", res);

}

void frmMain::SB(QString path)
{
    if(curScreen==NULL || !videoMax)
    {
        return;
    }

    BOOL ok = NET_DVR_CapturePictureBlock(
      deviceMap.value(curScreen),
     const_cast<char*>("D:\\cl3.jpg"),
      0
    );

    if(ok)
    {
        IplImage *img1 =  cvLoadImage("D:\\cl3.jpg");;
        ASF_MultiFaceInfo detectedFaces1 = { 0 };
        ASF_SingleFaceInfo SingleDetectedFaces1 = { 0 };

        ASF_FaceFeature feature1 = { 0 };
        ASF_FaceFeature copyfeature1 = { 0 };


        IplImage* cutImg1 = cvCreateImage(cvSize(img1->width - img1->width % 4, img1->height), IPL_DEPTH_8U, img1->nChannels);

        CutIplImage(img1, cutImg1, 0, 0);
        int res = ASFDetectFaces(handle, cutImg1->width, cutImg1->height, ASVL_PAF_RGB24_B8G8R8, (MUInt8*)cutImg1->imageData, &detectedFaces1);
        if (MOK == res)
        {
            cv::Mat m = cv::cvarrToMat(cutImg1);
            //
            for (int i = 0; i < detectedFaces1.faceNum; i++)
            {
                ;
                int mLeft =  detectedFaces1.faceRect[i].left;
                int mTop =   detectedFaces1.faceRect[i].top;
                int mRignt = detectedFaces1.faceRect[i].right;
                int mBottom =detectedFaces1.faceRect[i].bottom;
                cv::rectangle(m, cv::Rect(mLeft, mTop, mRignt - mLeft, mBottom - mTop), cv::Scalar(0, 0, 255));
            }
            qDebug()<<detectedFaces1.faceNum;
            cv::imshow("处理结果", m);
            cv::waitKey();
        }

    }

//    cvReleaseImage(&cutImg1);
}
