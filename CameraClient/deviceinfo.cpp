﻿#include "deviceinfo.h"

DeviceInfo::DeviceInfo()
{

}

int DeviceInfo::getId()
{
    return id;
}

QString DeviceInfo::getDeviceAddress()
{
    return deviceAddress;
}

int DeviceInfo::getDevicePort()
{
    return devicePort;
}

QString DeviceInfo::getDeviceUsername()
{
    return deviceUsername;
}

QString DeviceInfo::getDevicePassword()
{
    return devicePassword;
}

QString DeviceInfo::getStreamAddress()
{
    return streamAddress;
}

void DeviceInfo::setId(int id)
{
    this->id = id;
}

void DeviceInfo::setStreamAddress(QString streamAddress)
{
    this->streamAddress = streamAddress;
}

void DeviceInfo::setDeviceAddress(QString deviceAddress)
{
    this->deviceAddress = deviceAddress;
}

void DeviceInfo::setDevicePort(int port)
{
    this->devicePort = port;
}

void DeviceInfo::setDeviceUsername(QString deviceUsername)
{
    this->deviceUsername = deviceUsername;
}

void DeviceInfo::setDevicePassword(QString devicePassword)
{
    this->devicePassword = devicePassword;
}


QString DeviceInfo::toString()
{
    QString toS = QString("[id=%1,username=%2,password=%3,port=%4,streamAddress=%5]")
            .arg(this->id).arg(this->deviceUsername).arg(this->devicePassword)
            .arg(this->devicePort).arg(this->streamAddress);
    return toS;
}

