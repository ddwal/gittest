﻿#ifndef TABWID_H
#define TABWID_H

#include <QWidget>

namespace Ui {
class TabWid;
}

class TabWid : public QWidget
{
    Q_OBJECT

public:
    explicit TabWid(QWidget *parent = 0);
    ~TabWid();

public slots:
    void click();
signals:
    void btnCLick(QString title);

private:
    Ui::TabWid *ui;
};

#endif // TABWID_H
