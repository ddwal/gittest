﻿#include "userinfo.h"

UserInfo::UserInfo()
{

}

int UserInfo::getId()
{
    return id;
}

int UserInfo::getSFlag()
{
    return sFlag;
}

QString UserInfo::getSUsername()
{
    return sUsername;
}

QString UserInfo::getSPassword()
{
    return sPassword;
}

QString UserInfo::getSLastSignTime()
{
    return sLastSignTime;
}

void UserInfo::setId(int id)
{
    this->id = id;
}

void UserInfo::setSFlag(int flag)
{
    this->sFlag = flag;
}

void UserInfo::setSUsername(QString sUsername)
{
    this->sUsername = sUsername;
}

void UserInfo::setSPassword(QString sPassword)
{
    this->sPassword = sPassword;
}

void UserInfo::setSLastSignTime(QString sLastSignTime)
{
    this->sLastSignTime = sLastSignTime;
}

QString UserInfo::toString()
{
    QString toS = QString("[id=%1,sUsername=%2,sPassword=%3,flag=%4]")
            .arg(this->id).arg(this->sUsername).arg(this->sPassword)
            .arg(this->sFlag);
    return toS;
}
