﻿#include "storgewid.h"
#include "ui_storgewid.h"

StorgeWid::StorgeWid(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StorgeWid)
{
    ui->setupUi(this);
}

StorgeWid::~StorgeWid()
{
    delete ui;
}

void StorgeWid::on_toolButtonStorge_clicked()
{
    i++;
    QString sheetStr ="";
    if(i%2)
    {
        sheetStr = "QToolButton{border-image: url(:/new/icon/icon/icon/leftL.png);}"
                      "QToolButton:hover{border:2px;}";

    }else
    {
        sheetStr = "QToolButton{border-image: url(:/new/icon/icon/icon/leftR.png);}"
                      "QToolButton:hover{border:2px;}";
    }
    ui->toolButtonStorge->setStyleSheet(sheetStr);
    emit storge();
}
