﻿#ifndef USERINFO_H
#define USERINFO_H
#include <QString>


class UserInfo
{
public:
    UserInfo();
    int     getId();
    int getSFlag();
    QString getSUsername();
    QString getSPassword();
    QString getSLastSignTime();


    void setId(int id);
    void setSFlag(int flag );
    void setSUsername(QString sUsername);
    void setSPassword(QString sPassword);
    void setSLastSignTime(QString sLastSignTime);

    QString toString();

private:
    int     id;
    int     sFlag;
    QString sUsername;
    QString sPassword;
    QString sLastSignTime;



};

#endif // USERINFO_H
