﻿#ifndef DBUTIL_H
#define DBUTIL_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include "deviceinfo.h"
#include "userinfo.h"
class DBUtil
{
public:
    DBUtil();
    void initDb(const QString path,const QString type);
    bool initDb(const QString hostname, const QString username, const QString password, const int port, const QString dbName, const QString type="QMYSQL");
    QSqlDatabase getDb();
    QString DBUtil::getLastError();
    QSqlQuery getQuery();
    bool getStatu();
    void closeDb();
    QList<DeviceInfo*> getDeviceInfo();
    UserInfo *findUser(UserInfo *info);



private:
   QSqlDatabase db;
   QSqlQuery query;
   bool isCon;
   QList<DeviceInfo*> DeviceInfoes;


};

#endif // DBUTIL_H
